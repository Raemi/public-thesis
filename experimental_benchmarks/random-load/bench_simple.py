#!/usr/bin/python

import time, sys, thread

import random
import threading
from abstract_threading import Future
from __pypy__.thread import atomic

def work():
    i = 0
    while i < 10000:
        i += 1
    #r=xrange
    #for i in r(10000):
    #    pass


def do(load):
    thread.should_commit()
    with atomic:
        for w in load:
            # yield (w,)
            with atomic:
                w()




def load(threads):
    load = [work] * 200
    todo = [load] * 32

    while todo:
        fs = []
        mywork = todo.pop()
        for _ in xrange(threads-1):
            if todo:
                fs.append(threading.Thread(target=do, args=(todo.pop(),)))
        [f.start() for f in fs]
        do(mywork)
        #time.sleep(10)
        [f.join() for f in fs]


def run(ths=16):
    load(int(ths))


    
if __name__ == '__main__':
    run()
