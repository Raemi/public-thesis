#!/usr/bin/python


from abstract_transactions import (protected, unprotected,
                                   Future, penalize, validate)

import time, sys, thread
import random


@protected
def atomic_unshared():
    for _ in xrange(10000):
        pass

counter = 0
@penalize()
@protected
def atomic_shared():
    global counter
    for _ in xrange(4000):
        counter += 1

@unprotected
def nonatomic_unshared():
    for _ in xrange(10000):
        pass


@unprotected
def nonatomic_shared():
    global counter
    for _ in xrange(4000):
        counter += 1

@unprotected
def random_load(jobs):
    for job in jobs:
        yield (job, )


@unprotected
def load(seed, threads, kind):
    #sys.settrace(trace_calls)
    r = random.Random(seed)
    loads = {
        'all':[atomic_unshared, atomic_shared,
               nonatomic_unshared, nonatomic_shared] * 10,
        'unshared':[atomic_unshared,
                    nonatomic_unshared] * 10,
        'shared':[atomic_shared,
                  nonatomic_shared] * 10
        }
    load = loads[kind]

    r.shuffle(load)

    work = [(random_load, load)] * 12
    while work:
        fs = []
        for _ in xrange(threads):
            if not work:
                break
            fs.append(Future(*work.pop()))
        [f() for f in fs]


def trace_calls(frame, event, arg):
    if event != 'call':
        return
    thread.log_checkpoint("BLA")


def main(ths=4, kind='all'):
    load(12, ths, kind).next()

def run(ths=4, kind='all'):
    main(int(ths), kind)

if __name__ == '__main__':
    main()
