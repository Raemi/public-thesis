#!/usr/bin/python

from abstract_threading import Future
import thread
from thread import atomic
import collections
import string, sys, time


class Node(object):
    pass


class ElementAdd(Node):
    def __init__(self):
        self.next = []

    def do(self, in1, in2):
        with atomic:
            out = []
            for i, j in zip(in1, in2):
                out.append(i + j)
        return [Future(self.next[0].do, out)]

class Dup(Node):
    def __init__(self):
        self.next = []

    def do(self, in1):
        with atomic:
            dup = in1[:]
        if len(self.next) == 1:
            return [Future(self.next[0].do, in1, dup)]
        return [Future(self.next[0].do, in1),
                Future(self.next[1].do, dup)]

class Result(Node):
    def do(self, in1):
        self.result = in1
        return self

def _wait_for_result(futures):
    if isinstance(futures, Result):
        return futures

    todo = []
    for f in futures:
        todo.append(f())
    for t in todo:
        res = _wait_for_result(t)
        if isinstance(res, Result):
            return res

def eval(first_node, *args):
    return _wait_for_result(first_node.do(*args))



def main():
    xs = [1]*10000
    first = Dup()
    add = ElementAdd()
    add.next.append(Result())
    first.next.append(add)
    print (eval(first, xs).result)

if __name__ == '__main__':
    main()
