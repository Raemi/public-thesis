#!/usr/bin/python

from abstract_threading import Future
import thread
from __pypy__.thread import atomic
import string, sys

noise = string.punctuation + string.whitespace
def process_text(text):
    words = text.lower().split()
    for w in words:
        res = w.strip(noise)
        if len(res) > 4 and res.isalpha():
            yield res

def process_file(filename, freqs):
    print "Start", filename

    with open(filename) as f:
        text = f.read()

    ## def insert_into_dict(w):
    ##     freqs[w] = freqs.get(w, 0) + 1

    #words = [w for w in process_text(text)]
    with atomic:
        words = list(process_text(text))
    print "file parsed"

    for word in words:
        #thread.at_commit(insert_into_dict, (word,))
        with atomic:
            freqs[word] = freqs.get(word, 0) + 1

    print "Finished", filename


def main():
    import os

    N = 1
    files = os.listdir('data')[:N]
    files.extend(files)

    from stm_dict import BoxingDict, ProxyDict, SplittingDict
    if args.boxing:
        freqs = BoxingDict()
    elif args.proxy:
        freqs = ProxyDict()
    elif args.splitting:
        freqs = SplittingDict()
    else:
        print "Need program argument!!"
        return

    res = []
    for f in files:
        res.append(Future(process_file,
                          os.path.join('data', f), freqs))
        if args.serial:
            res[0]()
            del res[0]
    [f() for f in res]

    if args.splitting:
        print "Splits:", zip(freqs._dicts.keys(), map(len, freqs._dicts.values()))



if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    flag='store_true'
    parser.add_argument('--boxing', action=flag, help='use boxing')
    parser.add_argument('--proxy', action=flag, help='use proxy')
    parser.add_argument('--splitting', action=flag, help='use splitting')
    parser.add_argument('--serial', action=flag, help='do serial execution')

    args = parser.parse_args()
    main()
