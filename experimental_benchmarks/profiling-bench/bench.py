#!/usr/bin/python


from abstract_transactions import (protected, unprotected,
                                   Future, penalize, validate)

import time, sys, thread
import random, math

@protected
def produce_numbers(count=1000):
    r = random.Random(3)
    res = []
    for _ in xrange(count):
        res.append(r.randint(0, 100))

    return res


def fib(n):
    a, b = 0, 1
    for i in range(n):
        a, b = b, a + b
    return a

@protected
def calc_list(xs, ys, zs):
    xs[0], ys[0], zs[0] = ys[0], xs[0], zs[0]
    thread.should_commit()
    thread.log_checkpoint("sdf")
    yield
    for i in xrange(len(xs)):
        xs[i] = fib(200)
    thread.log_checkpoint("sdf")



@unprotected
def load(PN, TN, num):
    res = []
    while PN > 0:
        fs = []
        for _ in xrange(TN):
            if PN > 0:
                fs.append(Future(produce_numbers, num))
                PN -= 1
        res.extend((f() for f in fs))


    zs = zip(res, res[1:] + res[:1], res[2:] + res[:2])
    while zs:
        fs = []
        for _ in xrange(TN):
            if zs:
                fs.append(Future(calc_list, *zs.pop()))
        [f() for f in fs]



def run(TN=4, PN=100, num=100):
    load(int(PN), int(TN), int(num)).next()

if __name__ == '__main__':
    run()
