#!/bin/sh

echo "WITHOUT PENALTY"
PYPYLOG=stm-log:stm_log1.log pypy-without-penalty app.py 2> /dev/null &
sleep 2
openload -l 8 127.0.0.1:5000 4
kill -9 %1

echo "WITH PENALTY"
PYPYLOG=stm-log:stm_log2.log pypy-c app.py 2> /dev/null &
sleep 2
openload -l 8 127.0.0.1:5000 4
kill -9 %1
