from werkzeug.wrappers import Request, Response
import thread
from __pypy__.thread import atomic

@Request.application
def application(request):
    #thread.should_commit()
    with atomic:
        i = 1000000
        while i:
            i -= 1
    return Response('Hello World!')

if __name__ == '__main__':
    from werkzeug.serving import run_simple
    run_simple('0.0.0.0', 5000, application,
               threaded=True, processes=1)
