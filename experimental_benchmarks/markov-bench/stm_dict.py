
class MyDict(object):
    def setdefault(self, k, v=None):
        if k in self:
            return self[k]
        self[k] = v
        return v

class BoxingDict(MyDict):
    def __init__(self):
        self._metadict = {}

    def __setitem__(self, k, v):
        md = self._metadict
        if k in md:
            md[k][0] = v
        else:
            md[k] = [v]

    def __getitem__(self, k):
        return self._metadict[k][0]

    def __contains__(self, k):
        return k in self._metadict

    def items(self):
        return [(k, v[0]) for k, v in self._metadict.items()]

    def pop(self, k, *args):
        if args:
            return self._metadict.pop(k, [args[0]])[0]
        return self._metadict.pop(k)[0]

    def get(self, k, *args):
        try:
            return self._metadict[k][0]
        except KeyError:
            if args:
                return args[0]
            raise


class ProxyDict(MyDict):
    def __init__(self):
        self._metadict = {}
    def __setitem__(self, k, v):
        self._metadict[k] = v
    def __getitem__(self, k):
        return self._metadict[k]
    def __contains__(self, k):
        return k in self._metadict
    def items(self):
        return self._metadict.items()
    def pop(self, k, *args):
        return self._metadict.pop(k, *args)
    def get(self, k, *args):
        return self._metadict.get(k, *args)


import collections, thread

class SplittingDict(MyDict):
    def __init__(self):
        self._dicts = collections.defaultdict(dict)
    def __setitem__(self, k, v):
        self._dicts[k[0]][k] = v
        #if v > 100: get contention on element and propose to commit if highly
        #            contended
        #    thread.should_commit()
    def __getitem__(self, k):
        return self._dicts[k[0]][k]
    def __contains__(self, k):
        return k in self._dicts[k[0]]
    def get(self, k, *args):
        return self._dicts[k[0]].get(k, *args)
