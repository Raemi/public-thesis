#!/usr/bin/python

from abstract_threading import Future
import thread
import collections
import string, sys, time
from stm_dict import ProxyDict, SplittingDict, BoxingDict
dict = BoxingDict

noise = string.punctuation + string.whitespace
def process_text(text):
    words = text.lower().split()
    #print len(words)
    #time.sleep(0.1)
    #thread.should_commit()

    for w in words:
        res = w.strip(noise)
        if len(w) > 3 and res.isalpha():
            yield res

## def update_props(props, words):
##     p = props
##     entry = None
##     for word in words:
##         if p is None:
##             p = dict()
##             entry[1] = p

##         entry = p.setdefault(word, [0, None])
##         entry[0] += 1
##         p = entry[1]


def update_props(props, words):
    p = props
    length = len(words)
    for i, word in enumerate(words):
        if i < length - 1:
            p = p.setdefault(word, dict())
        else:
            p[word] = None
            break

def process_file(filename, props, depth=2):
    print "Start", filename

    with open(filename) as f:
        text = f.read()

    words = process_text(text)
    buf = collections.deque(maxlen=depth)
    # crashes if < depth-1 words
    for _ in range(depth - 1):
        buf.append(words.next())

    for word in words:
        buf.append(word)
        with thread.atomic:
            update_props(props, buf)

    print "Finished", filename


def main():
    import os

    N = 2
    files = os.listdir('data')[:N]

    props = dict()
    res = []
    for f in files:
        res.append(Future(process_file,
                          os.path.join('data', f), props))
        if args.serial:
            res[0]()
            del res[0]

    [f() for f in res]

    #import pprint
    #pprint.pprint(props)




if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    flag='store_true'
    parser.add_argument('--serial', action=flag, help='do serial execution')

    args = parser.parse_args()
    main()
