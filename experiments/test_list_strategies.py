
import time, sys
import thread, threading
from __pypy__.thread import atomic

####################################################################
def appender(l, n, i=1):
    for _ in xrange(n):
        l.append(i)
    #l[0]
def setter(l, low, high, i=1):
    with atomic:
        for c in xrange(low, high):
            l[c] = i
####################################################################

def create_threads(TN, SIZE, func, *args):
    res = []
    ranges = (i for i in xrange(0, SIZE, SIZE / TN))
    for i in xrange(TN):
        iargs = list(args)
        low = ranges.next()
        iargs.append(low)
        iargs.append(low + SIZE/TN)
        iargs.append(i)
        res.append(threading.Thread(target=func, args=iargs))
    return res


sys.setcheckinterval(50000)
print "CHECK_INTERVAL:", sys.getcheckinterval()

thread.start_new_thread(lambda:0, ())

SIZE = 1500000
TN = 20

time.sleep(0.2)

ti = time.time()
shared_list = [0] * SIZE
ts = create_threads(TN, SIZE, setter, shared_list)
[t.start() for t in ts]
[t.join() for t in ts]
ti = time.time() - ti
time.sleep(0.1)
print "RESULT:", len(shared_list), ti
