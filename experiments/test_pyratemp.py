import pyratemp
import thread, testing

RUNS = 20
TEMPL = 100

def func():
    t = pyratemp.Template(filename="example.html")
    result = t(title="pyratemp is simple!",
               special_chars=u"""<>"'&äöü""",
               number=42,
               mylist=("Spam", "Parrot", "Lumberjack"))
    result.encode("ascii", 'xmlcharrefreplace')
    

t = testing.run(RUNS, func, K=3)
print "RESULT", min(t)


t = testing.run_future(RUNS, func, K=3)
print "RESULT", min(t)
