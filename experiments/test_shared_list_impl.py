
import time, sys
import thread, threading
from abstract_threading import AtomicFuture

########################## shared counter ##########################
from thread import atomic, get_ident

class STMSharedList(object):
    def __init__(self, length, stripes=1):
        self.stripe_count = stripes
        self.stripes = [[0] * (length // stripes) for i in xrange(stripes)]
        self.stripes[-1].extend([0] * (length % stripes))
        self.lens = [len(s) for s in self.stripes]

    def __len__(self):
        return sum(self.lens)

    def __getitem__(self, i):
        with atomic:
            ind = 0
            for s, l in enumerate(self.lens):
                if ind + l <= i:
                    ind += l
                else:
                    return self.stripes[s][i-ind]
            else:
                raise IndexError

    def __setitem__(self, i, v):
        with atomic:
            ind = 0
            for s, l in enumerate(self.lens):
                if ind + l <= i:
                    ind += l
                else:
                    self.stripes[s][i-ind] = v
                    return
            else:
                raise IndexError

    def to_list(self):
        res = []
        for s in self.stripes:
            res.extend(s)
        return res

    def __repr__(self):
        return repr(self.to_list())


####################################################################
def setter(l, start, end, n):
    for _ in range(n):
        for i in range(start, end):
            l[i] += 1


def create_threads(tn, cn, sl):
    res = []
    sllen = len(sl)
    for i in range(tn):
        res.append(
            threading.Thread(target=setter,
                             args=(sl,
                                   i*(sllen//tn),
                                   (i+1)*(sllen//tn),
                                   cn
                                   )))
    return res



TN = 20
N = 10
LEN = 1000
ST = [1, 2, 4, 8, 16, 32, 64]

# K-Best timing
K = 5

results = []
for stripes in ST:
    timings = []
    while len(timings) < K:
        sl = STMSharedList(LEN, stripes)
        ts = create_threads(TN, N, sl)

        print "#### Start with %s stripes ####" % (stripes,)

        time.sleep(0.2)
        ti = time.time()
        for t in ts:
            t.start()
        for t in ts:
            t.join()
        ti = time.time() - ti
        print "RESULT:", ti, len(sl)
        timings.append(ti)
    results.append(min(timings))

print results
