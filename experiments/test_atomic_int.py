
import time, sys
import thread, threading
from abstract_threading import AtomicFuture

########################## shared counter ##########################
from thread import atomic, get_contention, get_ident
import gc
class STMSharedInteger(object):
    def __init__(self, v=0):
        self._v = v
        self.stripe_count = 0
        self.stripes = []

    def __add__(self, v):
        #print "", len(gc.get_referents(self.__dict__))
        get_contention(self.__dict__)
        with atomic:
            if not self.stripe_count:
                self._v += v
            else:
                i = get_ident() % self.stripe_count
                self.stripes[i][0] += v
        return self

    def get(self):
        with atomic:
            if self.stripe_count:
                for s in self.stripes:
                    self._v += s[0]
                    s[0] = 0
            return self._v

    def set(self, v):
        with atomic:
            if self.stripe_count:
                for s in self.stripes:
                    s[0] = 0
            self._v = v

####################################################################
def adder(s, n):
    for i in range(n):
        s += 1

def appender(l, n):
    for i in range(n):
        l.append('a')
        l.append(1)


time.sleep(0.2)

t = time.time()

si = STMSharedInteger(0)
t1 = threading.Thread(target=adder, args=(si, 100))
t2 = threading.Thread(target=adder, args=(si, 100))
t3 = threading.Thread(target=adder, args=(si, 100))
t1.start()
t2.start()
t3.start()
t1.join()
t2.join()
t3.join()
l = list()
## t1 = threading.Thread(target=appender, args=(l, 50000))
## t2 = threading.Thread(target=appender, args=(l, 50000))
## t3 = threading.Thread(target=appender, args=(l, 50000))
## t1.start()
## t2.start()
## t3.start()
## t1.join()
## t2.join()
## t3.join()

## t = time.time() - t

## print "RESULT", len(l), map(lambda x:(type(x[0]), x[1]), get_contention(l))
print "RESULT", si.get(), t
print map(lambda x:(str(x[0]), x[1]), get_contention(si.__dict__))
