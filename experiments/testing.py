
import time
from abstract_threading import Future
import thread

# ensure transaction-limit is set also for single-threaded runs
thread.start_new_thread(lambda:1, ())

def run(N, test, testargs=(), testkwargs={}, K=3,
        init=None, initargs=(), initkwargs={}):
    times = []
    time.sleep(0.2)
    for k in xrange(K):
        if init is not None:
            init(*initargs, **initkwargs)

        time.sleep(0.1)

        t = time.time()
        for n in xrange(N):
            test(*testargs, **testkwargs)
        t = time.time() - t
        times.append(t)
    return times


def run_future(N, test, testargs=(), testkwargs={}, K=3,
        init=None, initargs=(), initkwargs={}):
    times = []
    time.sleep(0.2)
    for k in xrange(K):
        if init is not None:
            init(*initargs, **initkwargs)

        time.sleep(0.1)

        t = time.time()
        fs = []
        for n in xrange(N):
            fs.append(Future(test, *testargs, **testkwargs))
        [f() for f in fs]
        t = time.time() - t
        times.append(t)
    return times
