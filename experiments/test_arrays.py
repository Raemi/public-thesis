
import thread
import time, sys


from abstract_threading import Future
############################################

def map_grid(grid, ul, lr, func):
    x1, y1 = ul
    x2, y2 = lr

    for x in xrange(x1, x2):
        for y in xrange(y1, y2):
            grid[x][y] = func(grid[x][y])

    return grid

def map_grid_parallel(grid, ul, lr, func, ps=100):
    x1, y1 = ul
    x2, y2 = lr

    results = []
    for x in range(x1, x2, ps):
        for y in range(y1, y2, ps):
            lr_patch = min(x2, x+ps), min(y2, y+ps)
            f = Future(map_grid, grid, (x,y), lr_patch, func)
            results.append(f)

    # wait for results:
    [f() for f in results]
    return grid

############################################
class _Box(object):
    def __init__(self, v=None):
        self.v = v
    def __repr__(self):
        return repr(self.v)

class ObjectList(list):
    def __init__(self):
        self.impl = []

    def __len__(self):
        return len(self.impl)

    def __getitem__(self, key):
        return self.impl[key].v

    def __setitem__(self, key, v):
        self.impl[key].v = v

    def __delitem__(self, key):
        del self.impl[key]

    def append(self, v):
        self.impl.append(_Box(v))

    def extend(self, other):
        raise NotImplemented

    def insert(self, i, v):
        self.impl.insert(i, _Box(v))

    def remove(self, x):
        raise NotImplemented

    def pop(self, o=None):
        raise NotImplemented

    def index(self, o):
        raise NotImplemented

    def count(self, o):
        raise NotImplemented

    def sort(self):
        raise NotImplemented

    def reverse(self):
        raise NotImplemented

    def __repr__(self):
        return repr(self.impl)

############################################



sys.setcheckinterval(50000)

print "CHECK_INTERVAL:", sys.getcheckinterval()


# enable reads_limit
thread.start_new_thread(lambda : 0, ())

#mxn
m, n = 300, 300
func = lambda x: (x**2 + x*3)**2

grid = [[x]*n for x in xrange(m)]
object_grid = ObjectList()
for x in xrange(m):
    o = ObjectList()
    object_grid.append(o)
    for y in xrange(n):
        o.append(x)



time.sleep(0.2)

t = time.time()
res2 = map_grid_parallel(grid, (0,0), (m,n), func)
t = time.time() - t
print "MAP_PARALLEL:", t



####################### obj list #####

time.sleep(0.2)



t = time.time()
res2 = map_grid_parallel(object_grid, (0,0), (m,n), func)
t = time.time() - t
print "MAP_PARALLEL:", t

#print "EQUAL:", repr(res1) == repr(res2)






##################### CONCLUSIONS ######################
# Since the normal grid is already a list of lists,
# there is some non-conflicting parallelism possible.
#
# The ObjectList shows no conflicts happening because
# of grid-access, but is slower because of indirection.
# This may be minimized by using the JIT.
########################################################
