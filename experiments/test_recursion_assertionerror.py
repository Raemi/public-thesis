

from threading import Thread

def parallel():
    t1 = Thread(target=parallel)
    t2 = Thread(target=parallel)
    
    t1.start()
    t2.start()
    
    t1.join()
    t2.join()
    

parallel()
