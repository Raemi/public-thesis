

from abstract_transactions import (protected, unprotected,
                                   Future,
                                   END_ATOMIC, RETURN)
import time, sys

@protected
def atomic_inc(i):
    return (RETURN, i + 1)

@unprotected
def print_it(text):
    sys.stdout.write(str(text) + "\n")


@protected
def trans(u):
    for i in xrange(100000):
        pass

    yield END_ATOMIC
    time.sleep(0.1)
    yield

    for i in range(50000):
        pass

    res = yield (atomic_inc, (u,))

    yield (print_it, (res,))

    yield END_ATOMIC
    if u % 2:
        Future(trans, u + 1)


def main():
    fs = []
    for i in range(15):
        fs.append(Future(trans, i))
    [f() for f in fs]


if __name__ == '__main__':
    main()
