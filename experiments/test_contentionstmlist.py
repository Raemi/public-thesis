
import time, sys
import thread, threading

from contentionstmlist import STMList, NonSTMList



####################################################################
def appender(l, n, i=1):
    for _ in xrange(n):
        l.append(i)
    #l[0]

def adder(l, id=0):
    if id % 2:
        it = xrange(0, len(l) / 2)
    else:
        it = xrange(len(l) / 2, len(l))

    for i in it:
        l[i] += 1
####################################################################

def create_threads(N, func, *args):
    res = []
    for i in range(N):
        iargs = list(args)
        iargs.append(i)
        res.append(threading.Thread(target=func, args=iargs))
    return res


sys.setcheckinterval(50000)
print "CHECK_INTERVAL:", sys.getcheckinterval()

thread.start_new_thread(lambda:0, ())

N = 5000
TN = 32

time.sleep(0.2)

def main():
    shared_list = NonSTMList()
    for n in xrange(N):
        shared_list.append(0)

    ti = time.time()
    ts = create_threads(TN, adder, shared_list)
    [t.start() for t in ts]
    [t.join() for t in ts]
    ti = time.time() - ti
    time.sleep(0.1)
    print "RESULT:", len(shared_list), ti
    print "cont:",shared_list._shared_list.get_contention()

    time.sleep(0.2)

    shared_list = STMList(N, 32)
    ti = time.time()
    ts = create_threads(TN, adder, shared_list)
    [t.start() for t in ts]
    [t.join() for t in ts]
    ti = time.time() - ti
    time.sleep(0.1)
    print "STM RESULT:", len(shared_list), ti



main()
