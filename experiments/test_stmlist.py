
import time, sys
import thread, threading

import stmlist



####################################################################
def appender(l, n, i=1):
    for _ in xrange(n):
        l.append(i)
    #l[0]
####################################################################

def create_threads(N, func, *args):
    res = []
    for i in range(N):
        iargs = list(args)
        iargs.append(i)
        res.append(threading.Thread(target=func, args=iargs))
    return res


sys.setcheckinterval(50000)
print "CHECK_INTERVAL:", sys.getcheckinterval()

thread.start_new_thread(lambda:0, ())

N = 10000
TN = 4

time.sleep(0.2)

ti = time.time()
shared_list = stmlist.NonSTMList()
ts = create_threads(TN, appender, shared_list, N)
[t.start() for t in ts]
[t.join() for t in ts]
ti = time.time() - ti
time.sleep(0.1)
print "RESULT:", len(shared_list), ti


time.sleep(0.2)

ti = time.time()
shared_list = stmlist.STMList()
ts = create_threads(TN, appender, shared_list, N)
[t.start() for t in ts]
[t.join() for t in ts]
ti = time.time() - ti
time.sleep(0.1)
print "RESULT:", len(shared_list), ti
