

import threading
from abstract_threading import Future

import random




def sort_list_parallel2(list):
    local = threading.local()

    def qsort_parallel2(list):
        if len(list) <= 1:
            return list
        #local = threading.local()
        rand = getattr(local, "random", None)
        if rand is None:
            rand = random.Random()
            local.random = rand
            
        r = rand.randrange(len(list))
        pivot = list.pop(r)
        left = [l for l in list if l < pivot]
        if len(list) > 500:
            lesser = Future(qsort_parallel2, left)
        else:
            lesser = lambda left=left: qsort_parallel2(left)

        greater = qsort_parallel2([l for l in list if l >= pivot])

        return lesser() + [pivot] + greater

    return qsort_parallel2(list[:])







xs = range(1, 10000)

ss = sort_list_parallel2(xs)






