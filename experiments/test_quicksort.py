
import time, sys
import threading, thread
from abstract_threading import Future

import random

def sort_list(list):
    def qsort(list):
        if list == []:
            return []
        else:
            r = random.randrange(len(list))
            pivot = list.pop(r)

            lesser = qsort([l for l in list if l < pivot])
            greater = qsort([l for l in list if l >= pivot])

            return lesser + [pivot] + greater

    return qsort(list[:])


def sort_list_parallel(list):
    def qsort_parallel(list):
        if list == []:
            return list

        r = random.randrange(len(list))
        pivot = list.pop(r)
        left = [l for l in list if l < pivot]
        if len(list) > 500:
            lesser = Future(qsort_parallel, left)
        else:
            lesser = lambda left=left: qsort_parallel(left)

        greater = qsort_parallel([l for l in list if l >= pivot])

        return lesser() + [pivot] + greater

    return qsort_parallel(list[:])


def sort_list_parallel2(list):
    local = threading.local()

    def qsort_parallel2(list):
        if list == []:
            return list

        rand = getattr(local, "random", None)
        if rand is None:
            rand = random.Random()
            local.random = rand

        r = rand.randrange(len(list))
        pivot = list.pop(r)
        left = [l for l in list if l < pivot]
        if len(list) > 500:
            lesser = Future(qsort_parallel2, left)
        else:
            lesser = lambda left=left: qsort_parallel2(left)
        
        greater = qsort_parallel2([l for l in list if l >= pivot])

        return lesser() + [pivot] + greater

    return qsort_parallel2(list[:])




sys.setcheckinterval(50000)
print "CHECK_INTERVAL:", sys.getcheckinterval()


xs = range(1, 10000)
random.shuffle(xs)

time.sleep(0.2)

t = time.time()
ss = sort_list_parallel(xs)
t = time.time() - t
print "SORTED_PARALLEL:", ss == sorted(xs), t

time.sleep(0.2)

t = time.time()
ss = sort_list_parallel2(xs)
t = time.time() - t
print "SORTED_PARALLEL2:", ss == sorted(xs), t

time.sleep(0.2)

t = time.time()
ss = sort_list(xs)
t = time.time() - t
print "SORTED_SERIAL:", ss == sorted(xs), t
