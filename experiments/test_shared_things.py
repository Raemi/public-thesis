
import time, sys
import thread
from abstract_threading import AtomicFuture

########################## shared counter ##########################
shared_counter = 0
def incr():
    global shared_counter
    shared_counter += 1


def incr_counter_serial(count):
    for _ in xrange(count):
        incr()
    return shared_counter

def incr_counter_parallel(count, steps=1000):
    res = []
    for i in xrange(0, count, steps):
        s = min(steps, count - i)
        res.append(AtomicFuture(incr_counter_serial, s))

    [f() for f in res]
    return shared_counter

########################## shared dict ##########################
shared_dict = {}

def write_to_dict(i):
    shared_dict[i] = i

def write_to_dict_serial(m, n):
    for i in xrange(m, n):
        write_to_dict(i)

def write_to_dict_parallel(m, n, steps=1000):
    res = []
    for i in xrange(m, n, steps):
        s = min(steps, n - i)
        res.append(AtomicFuture(write_to_dict_serial, i, i + s))

    [f() for f in res]

####################################################################



sys.setcheckinterval(50000)
print "CHECK_INTERVAL:", sys.getcheckinterval()

thread.start_new_thread(lambda:0, ())

N = 100000

time.sleep(0.2)

t = time.time()
shared_dict = {}
write_to_dict_serial(0, N)
t = time.time() - t
print "WRITE_TO_DICT_SERIAL:", t, len(shared_dict)

time.sleep(0.2)


t = time.time()
shared_dict = {}
write_to_dict_parallel(0, N)
t = time.time() - t
print "WRITE_TO_DICT_PARALLEL:", t, len(shared_dict)

###########################################################

time.sleep(0.2)

t = time.time()
shared_counter = 0
res = incr_counter_serial(N)
t = time.time() - t
print "INC_COUNTER_SERIAL:", res, t

time.sleep(0.2)


t = time.time()
shared_counter = 0
res = incr_counter_parallel(N)
t = time.time() - t
print "INC_COUNTER_PARALLEL:", res, t
