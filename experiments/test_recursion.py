
import thread
from threading import Thread
import threading
import time, sys
import transaction

#############################################
from abstract_threading import Future

###########################################

def fib(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    return fib(n-1) + fib(n-2)



def fib_parallel(n, limit=19):
   if n == 0:
       return 0
   elif n == 1:
       return 1

   if n >= limit:
       f1 = Future(fib_parallel, n-1, limit)
       f2 = fib_parallel(n-2)
       return f1() + f2

   return fib_parallel(n-1, limit) + fib_parallel(n-2, limit)



sys.setcheckinterval(50000)
transaction.set_num_threads(4)
print "CHECK_INTERVAL:", sys.getcheckinterval()


xs = range(1, 24)

# enable reads_limit
thread.start_new_thread(lambda : 0, ())


## t = time.time()
## res = [fib(x) for x in xs]
## t = time.time() - t
## print "FIB_SERIAL:", t, res


## t = time.time()
## with thread.atomic:
##     res = [fib(x) for x in xs]
## t = time.time() - t
## print "FIB_SERIAL_ATOMIC:", t, res


t = time.time()
res = [fib_parallel(x) for x in xs]
t = time.time() - t
print "FIB_PARALLEL:", t, res

t = time.time()
res = [fib(x) for x in xs]
t = time.time() - t
print "FIB_SERIAL:", t, res
