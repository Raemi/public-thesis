import tempita
import thread, testing

tmpl = r"""
some text

{{for x in y}}
  <br>{{x}}</br>
  {{if x%1==0}}<br>even</br>
  {{else}}<br>odd</br>
  {{endif}}
{{endfor}}

some text
"""



RUNS = 5


def func():
    t = tempita.Template(tmpl)
    
    t.substitute(y = range(100))


t = testing.run(RUNS, func, K=3)
print "RESULT", min(t)


t = testing.run_future(RUNS, func, K=3)
print "RESULT", min(t)
