
import time, sys
import thread
from abstract_threading import Future, AtomicFuture


####################################################################
from Queue import Queue


def get_element(q):
    return q.get()

def put_element(q, e):
    q.put(e)

def deadlock():
    q = Queue()

    with thread.atomic:
        q.get()

####################################################################
def sleepy():
    with thread.atomic:
        time.sleep(1.0)
def do_work():
    [i**2 for i in range(10000)]
####################################################################
def block_lock(l):
    l.acquire()
    time.sleep(1)
    l.release()

####################################################################




sys.setcheckinterval(50000)
print "CHECK_INTERVAL:", sys.getcheckinterval()

thread.start_new_thread(lambda:0, ())


time.sleep(0.2)

#deadlock()

thread.start_new_thread(sleepy, ())
thread.start_new_thread(do_work, ())
thread.start_new_thread(do_work, ())

# does not deadlock:
## l = thread.allocate_lock()
## thread.start_new_thread(block_lock, (l,))
## time.sleep(0.3)
## l.acquire()

