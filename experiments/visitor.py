from ast import NodeVisitor
from abstract_threading import Future
from stmlist import STMList, NonSTMList

class BaseNode(object):
    def __init__(self):
        self.children = []
class Constant(BaseNode):
    def __init__(self, value):
        BaseNode.__init__(self)
        self.value = value
class BinaryOp(BaseNode):
    def __init__(self, name):
        BaseNode.__init__(self)
        self.name = name
class BinaryPlus(BinaryOp):
    def __init__(self, c1, c2):
        BinaryOp.__init__(self, "+")
        self.children.extend([c1, c2])


def build_tree(r, level=10):
    rn = 2 if level <= 0 else r.random()
    if rn > 0.9:
        return Constant(1)
    return BinaryPlus(build_tree(r, level-1), build_tree(r, level-1))


class PrintVisitor(NodeVisitor):
    def __init__(self):
        self.out = []

    def visit_Constant(self, node):
        self.out.append(str(node.value))

    def visit_BinaryPlus(self, node):
        self.out.append("(+,")
        for c in node.children:
            self.visit(c)
            self.out.append(',')
        del self.out[-1]
        self.out.append(')')

def f():
    with thread.atomic:
        r = random.Random(3)
        build_tree(r, 10)


import thread, time, random
thread.start_new_thread(lambda:0, ())

N = 100

time.sleep(0.1)

t = time.time()
for _ in range(N):
    f()
t = time.time() - t
print "RESULT", t

time.sleep(0.1)

t = time.time()
def func():
    fs = []
    for _ in range(N):
        fs.append(Future(f))
    [y() for y in fs]
func()
t = time.time() - t
print "RESULT", t


## pv = PrintVisitor()
## pv.visit(v)
## print len("".join(pv.out))
