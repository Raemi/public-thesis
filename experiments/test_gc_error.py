
import time, sys
import thread, threading
from abstract_threading import AtomicFuture

########################## shared counter ##########################

import gc

def adder(n):
    for i in range(n):
        gc.get_referents(gc)

time.sleep(0.2)

t = time.time()

t1 = threading.Thread(target=adder, args=(100,))
t2 = threading.Thread(target=adder, args=(100,))
t3 = threading.Thread(target=adder, args=(100,))
t1.start()
t2.start()
t3.start()
t1.join()
t2.join()
t3.join()
