
import time, sys
import thread, threading
from abstract_threading import AtomicFuture





####################################################################
shared_list = []
def appender(n):
    for _ in range(n):
        shared_list.append(1)
####################################################################

def create_threads(N, func, *args):
    res = []
    for i in range(N):
        res.append(threading.Thread(target=func, args=args))
    return res

thread.start_new_thread(lambda:0, ())

N = 1000

time.sleep(0.2)

ti = time.time()
shared_list = []
ts = create_threads(20, appender, N)
[t.start() for t in ts]
[t.join() for t in ts]
ti = time.time() - ti
print "RESULT:", ti
