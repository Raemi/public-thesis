
import time, sys
import thread, threading
from abstract_threading import AtomicFuture

########################## shared counter ##########################
from thread import atomic, get_ident

class STMSharedInteger(object):
    def __init__(self, v=0, stripes=1):
        self.stripe_count = stripes
        self.stripes = [[0] for i in xrange(stripes)]
        self.stripes[0][0] = v

    def __add__(self, v):
        i = get_ident() / 9973
        with atomic:
            i = i % self.stripe_count
            self.stripes[i][0] += v
        return self

    def get(self):
        with atomic:
            l0 = self.stripes[0]
            for s in self.stripes[1:]:
                l0[0] += s[0]
                s[0] = 0
            return l0[0]

    def set(self, v):
        with atomic:
            for s in self.stripes:
                s[0] = 0
            self.stripes[0][0] = v

####################################################################
def adder(s, n):
    for i in range(n):
        s += 1

def create_threads(tn, cn, si):
    res = []
    for i in range(tn):
        res.append(threading.Thread(target=adder, args=(si, cn)))
    return res



TN = 20
N = 1000
ST = [1, 2, 4, 8, 16, 32, 64]

# K-Best timing
K = 5
print "Test increasing integer with %s threads each by %s." % (TN, N)


results = []
for stripes in ST:
    timings = []
    while len(timings) < K:
        si = STMSharedInteger(0, stripes)
        ts = create_threads(TN, N, si)

        print "#### Start with %s stripes ####" % (stripes,)

        time.sleep(0.2)
        ti = time.time()
        for t in ts:
            t.start()
        for t in ts:
            t.join()
        ti = time.time() - ti
        print si.stripes
        print "RESULT:", si.get(), ti
        timings.append(ti)
    results.append(min(timings))

print results
