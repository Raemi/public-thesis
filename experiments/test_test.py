
import time, sys
import thread
from abstract_threading import Future, AtomicFuture

sys.setcheckinterval(50000)
print "CHECK_INTERVAL:", sys.getcheckinterval()

thread.start_new_thread(lambda:0, ())


time.sleep(0.1)

def f():
    d = {}
    # NOK: A gets registered and weakref_create turns inevitable
    with thread.atomic:
        class A(object):
            pass
        d[A()] = A()
f()

def g():
    # works
    with thread.atomic:
        def b(): pass
        b()

#g()




