
from thread import atomic, at_commit, run_commit, get_ident
from threading import local


## THIS TEST IS WEIRD ##











def atomically(func):
    def inner(*args, **kwargs):
        with atomic:
            return func(*args, **kwargs)
    return inner



class STMList(object):
    def __init__(self, length, stripes=1):
        self.stripe_count = stripes
        self.stripes = [[0] * (length // stripes) for i in xrange(stripes)]
        self.stripes[-1].extend([0] * (length % stripes))
        self.lens = [len(s) for s in self.stripes]

    def __len__(self):
        return sum(self.lens)


    def append(self, v):
        with atomic:
            self.stripes[-1].append(v)
            self.lens[-1] += 1

    def _get_stripe(self, idx):
        ind = 0
        for s, l in enumerate(self.lens):
            if ind + l <= idx:
                ind += l
            else:
                return s, idx - ind


    def __getitem__(self, i):
        with atomic:
            s, idx = self._get_stripe(i)
            return self.stripes[s][idx]

    def __setitem__(self, i, v):
        with atomic:
            s, idx = self._get_stripe(i)
            self.stripes[s][idx] = v


    def to_list(self):
        res = []
        for s in self.stripes:
            res.extend(s)
        return res

    def __repr__(self):
        return repr(self.to_list())




class NonSTMList(object):
    def __init__(self):
        self._shared_list = []

    @atomically
    def append(self, item):
        self._shared_list.append(item)

    @atomically
    def __getitem__(self, idx):
        return self._shared_list[idx]

    @atomically
    def __setitem__(self, idx, v):
        self._shared_list[idx] = v

    @atomically
    def __len__(self):
        return len(self._shared_list)

    @atomically
    def __repr__(self):
        return repr(self._shared_list)



if __name__ == '__main__':
    # tests:
    l = STMList()
    l.append(1)
    l.__commit__()
    assert l[0]==1 and len(l)==1
    print l

    with atomic:
        l[0] = 2
        l.append(1)
        a = l[0]
        b = l[1]
        c = len(l)
    assert a==2 and b==1 and c==2
    print l

    l.append(3)
    assert len(l)==3
    print l
