
import time, sys
import thread
from abstract_threading import Future, AtomicFuture


####################################################################
import json # moving that into parse_file is evil. seems to register
# itself in some global data structure


def parse_file(file):
    #import json # don't do it here!
    with open(file) as f:
        return json.load(f, encoding="latin-1")


def parse_files():
    fs = ["json/data_"+str(i+1) for i in range(9)]*2

    res = []
    for f in fs:
        res.append(parse_file(f))

    return res

def parse_files_parallel():
    fs = ["json/data_"+str(i+1) for i in range(9)]*2

    res = []
    for f in fs:
        # no AtomicFuture because parse_file does I/O
        res.append(Future(parse_file, f))

    return [r() for r in res]

####################################################################




sys.setcheckinterval(50000)
print "CHECK_INTERVAL:", sys.getcheckinterval()

thread.start_new_thread(lambda:0, ())

N = 100000

time.sleep(0.2)

t = time.time()
res = parse_files()
t = time.time() - t
print "PARSE_SERIAL:", t

time.sleep(0.2)


t = time.time()
res = parse_files_parallel()
t = time.time() - t
print "PARSE_PARALLEL:", t
