
import thread
import time, sys
import transaction


def merge_lists(xs, ys):
    len_x, len_y = len(xs), len(ys)
    i, j = 0, 0
    result = []

    while i < len_x and j < len_y:
        if xs[i] <= ys[j]:
            result.append(xs[i])
            i += 1
        else:
            result.append(ys[j])
            j += 1

    if i < len_x:
        result.extend(xs[i:])
    else:
        result.extend(ys[j:])

    return result


def sort_list(xs):
    if len(xs) == 1:
        return xs

    half_length = len(xs) / 2
    ys = sort_list(xs[:half_length])
    zs = sort_list(xs[half_length:])

    return merge_lists(ys, zs)


#-------------------------


def sort_list_parallel(xs):
    parts = 8
    part_length = len(xs) / parts

    outs = []
    for i in xrange(parts):
        out = []
        outs.append(out)

        if i < parts - 1:
            part = xs[i*part_length:(i+1)*part_length]
        else:
            part = xs[i*part_length:]

        transaction.add(lambda out=out, part=part: out.extend(sort_list(part)))

    transaction.run()

    result = []
    for i in xrange(parts):
        result = merge_lists(result, outs[i])
    return result

#--------------------------







sys.setcheckinterval(50000)
transaction.set_num_threads(4)
print "CHECK_INTERVAL:", sys.getcheckinterval()


xs = range(1, 40000)
import random
random.shuffle(xs)

time.sleep(0.1)

t = time.time()
ss = sort_list_parallel(xs)
t = time.time() - t
print "SORTED_PARALLEL:", ss == sorted(xs), t

time.sleep(0.1)

t = time.time()
ss = sort_list(xs)
t = time.time() - t
print "SORTED_SERIAL:", ss == sorted(xs), t
