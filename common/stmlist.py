
from __pypy__.thread import atomic
from thread import at_commit, run_commit
from threading import local


def atomically(func):
    def inner(*args, **kwargs):
        with atomic:
            return func(*args, **kwargs)
    return inner

class STMList(object):
    def __init__(self):
        self._shared_list = []

    def append(self, item):
        at_commit(self._shared_list.append, (item,))

    def extend(self, other):
        at_commit(self._get_local_list().extend, (other,))

    @atomically
    def __getitem__(self, idx):
        # can possibly be made better:
        run_commit()
        return self._shared_list[idx]

    @atomically
    def __setitem__(self, idx, v):
        # can possibly be made better
        run_commit()
        self._shared_list[idx] = v

    @atomically
    def __len__(self):
        # can possibly be made better
        run_commit()
        return len(self._shared_list)

    @atomically
    def __repr__(self):
        # can possibly be made better
        run_commit()
        return repr(self._shared_list)



class STMListOld(object):
    '''
    Maintains a thread-local list where transactions
    append/extend to. This list is empty or non-existant
    between transactions.
    At commit time, it inevitably pushes the changes
    to the thread-shared list and empties the thread-local
    list.
    '''

    def __init__(self):
        self._shared_list = []
        self._local = local()

    def _get_local_list(self):
        return self._local.__dict__.setdefault('l', [])
        # try:
        #     return self._local.__dict__['l']
        # except AttributeError:
        #     self._local.__dict__['l']
        # return self._local.list

    @atomically
    def append(self, item):
        self._get_local_list().append(item)
        at_commit(self.__commit, ())

    @atomically
    def extend(self, other):
        self._get_local_list().extend(other)
        at_commit(self.__commit, ())

    @atomically
    def __getitem__(self, idx):
        # can possibly be made better:
        run_commit()
        return self._shared_list[idx]

    @atomically
    def __setitem__(self, idx, v):
        # can possibly be made better
        run_commit()
        self._shared_list[idx] = v

    @atomically
    def __len__(self):
        # can possibly be made better
        run_commit()
        return len(self._shared_list)

    @atomically
    def __repr__(self):
        # can possibly be made better
        run_commit()
        return repr(self._shared_list)


    def __commit(self):
        # will always run atomically
        l = self._get_local_list()
        if l:
            self._shared_list.extend(l)
            del l[:]

class NonSTMList(object):
    def __init__(self):
        self._shared_list = []

    @atomically
    def append(self, item):
        self._shared_list.append(item)

    @atomically
    def extend(self, other):
        self._shared_list.extend(other)

    @atomically
    def __getitem__(self, idx):
        return self._shared_list[idx]

    @atomically
    def __setitem__(self, idx, v):
        self._shared_list[idx] = v

    @atomically
    def __len__(self):
        return len(self._shared_list)

    @atomically
    def __repr__(self):
        return repr(self._shared_list)



if __name__ == '__main__':
    # tests:
    l = STMList()
    l.append(1)
    run_commit()
    assert l[0]==1 and len(l)==1
    print l

    with atomic:
        l[0] = 2
        l.append(1)
        a = l[0]
        b = l[1]
        c = len(l)
    assert a==2 and b==1 and c==2
    print l

    l.append(3)
    assert len(l)==3
    print l
