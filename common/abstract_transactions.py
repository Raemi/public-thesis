
from Queue import Queue, Empty, Full
from threading import Thread, Condition
import thread
from __pypy__.thread import atomic, is_atomic

class Worker(Thread):
    """Thread executing tasks"""
    def __init__(self, queue):
        Thread.__init__(self)
        self.daemon = True
        self.next_task = None
        self.cond = Condition()
        self.queue = queue
        self.start()

    def run(self):
        while True:
            with self.cond:
                while self.next_task is None:
                    self.cond.wait()

                func, args, kargs = self.next_task
                self.next_task = None

                try:
                    func(*args, **kargs)
                except Exception as e:
                    print e

            try:
                self.queue.put_nowait(self)
            except Full:
                # thread limit reached, I'll show myself out..
                return


class ThreadPool(object):
    def __init__(self, thread_queue_size=12):
        self.threads = Queue(thread_queue_size)

    def add_task(self, func, *args, **kargs):
        try:
            worker = self.threads.get_nowait()
        except Empty:
            worker = Worker(self.threads)

        with worker.cond:
            worker.next_task = (func, args, kargs)
            worker.cond.notify_all()

import multiprocessing
_thread_pool = ThreadPool(2 * multiprocessing.cpu_count())


### Futures ###

BEGIN_ATOMIC = 1
END_ATOMIC = 2
RETURN = 3

class Future(object):
    def __init__(self, func, *args, **kwargs):
        self._done = False
        self._result = None
        self._exception = None
        self._cond = Condition()

        assert hasattr(func, "__call__")
        assert hasattr(func, '__protected__') \
          or hasattr(func, '__unprotected__')

        _thread_pool.add_task(self._task, func, *args, **kwargs)


    def _task(self, func, *args, **kwargs):
        with self._cond:
            try:
                self._result = self._call(func, args, kwargs)
            except Exception as e:
                self._exception = e
            finally:
                self._done = True
                # several points/threads in the program
                # may wait for the result (notify_all):
                self._cond.notify_all()


    def _call(self, func, args=(), kargs={}):
        assert hasattr(func, '__protected__') \
          or hasattr(func, '__unprotected__')

        atomically = hasattr(func, '__protected__')

        try:
            gen = func(*args, **kargs)
            res = None
            inner_res = None
            while True:
                if (atomically and res is None) or res is BEGIN_ATOMIC:
                    #thread.add_penalty(1000, 0)
                    #thread.should_commit()
                    with atomic:
                        res = gen.send(inner_res)
                elif (not atomically and res is None) or res is END_ATOMIC:
                    assert not is_atomic(), \
                      "Calling unprotected code from protected " \
                      "context is not allowed."
                    res = gen.send(inner_res)
                elif isinstance(res, tuple):
                    if res[0] is RETURN:
                        return res[1]
                    try:
                        inner_res = self._call(*res)
                        res = None # call with inner_res
                        continue
                    except StopIteration:
                        raise
                    except Exception as e:
                        if atomically:
                            with atomic:
                                res = gen.throw(e)
                        else:
                            res = gen.throw(e)
                else:
                    res = gen.send(inner_res)
                inner_res = None
        except StopIteration:
            pass

        return res


    def __call__(self):
        with self._cond:
            while not self._done:
                self._cond.wait()

        if self._exception:
            raise self._exception

        return self._result


### DECORATORS ###
from functools import wraps
import inspect

def protected(f):
    @wraps(f)
    def inner_gen(*args, **kwargs):
        if not is_atomic():
            raise Exception("Protected function needs to be called "\
                            "from protected/atomic context "+str(f))
        yield f(*args, **kwargs)
    #
    @wraps(f)
    def inner(*args, **kwargs):
        # can't check for atomic on instantiation of generator
        return f(*args, **kwargs)
    #
    res = inner
    if not inspect.isgeneratorfunction(f):
        res = inner_gen
    res.__protected__ = True
    return res


def unprotected(f):
    @wraps(f)
    def inner_gen(*args, **kwargs):
        if is_atomic():
            raise Exception("Unprotected function needs to be called "\
                            "from unprotected/non-atomic context "+str(f))
        yield f(*args, **kwargs)
    #
    @wraps(f)
    def inner(*args, **kwargs):
        # can't check for atomic on instantiation of generator
        return f(*args, **kwargs)
    #
    res = inner
    if not inspect.isgeneratorfunction(f):
        res = inner_gen
    res.__unprotected__ = True
    return res


def penalize(penalty=1000):
    def really_decorate(f):
        @wraps(f)
        def inner(*args, **kwargs):
            thread.add_penalty(penalty, 0)
            try:
                res = f(*args, **kwargs)
            finally:
                thread.add_penalty(penalty, 0)
            return res

        return inner
    return really_decorate

def validate(f):
    @wraps(f)
    def inner(*args, **kwargs):
        thread.log_checkpoint("VALIDATE_ENTER({})".format(f))
        try:
            res = f(*args, **kwargs)
        finally:
            thread.log_checkpoint("VALIDATE_LEAVE({})".format(f))
        return res
    return inner
