#!/bin/sh

echo "" > setcheck.log 
for check in 10 100 1000 10000 100000 1000000 10000000 ; do
    echo $check
    echo "== $check ==" >> setcheck.log
    PYPYLOG=stm-log:stm_setcheck_$check.log pypy-c-contention-penalty-no-scenarios app_setcheck.py --check $check 2> /dev/null &
    sleep 3
    openload -l 8 127.0.0.1:5000 4 2>> setcheck.log >> setcheck.log
    killall -9 pypy-c-contention-penalty-no-scenarios
done;
