#!/bin/bash

plot_log.py stm_no_penalty.log --legend --no-rma --pdf stm_flask_bad.pdf --threads 1-5 --no-should-commit
plot_log.py stm_should_commit.log --legend --no-rma --pdf stm_flask_should_commit.pdf --threads 1-5
plot_log.py stm_penalty.log --legend --no-rma --pdf stm_flask_good.pdf --threads 1-5 --no-should-commit
./plot_setcheck.py --figure-size 6x2 
