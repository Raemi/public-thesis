from flask import Flask
app = Flask(__name__)

import thread
from __pypy__.thread import atomic

@app.route("/")
def hello():
    with atomic:
        i = 1000000
        while i:
            i -= 1
    return 'Hello World!'


if __name__ == "__main__":
    import sys
    sys.setcheckinterval(100000)
    app.run(threaded=True, host='0.0.0.0')
