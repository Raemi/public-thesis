#!/bin/sh

echo "WITHOUT PENALTY"
PYPYLOG=stm-log:stm_no_penalty.log pypy-c-limit-halving app.py 2> /dev/null &
sleep 2
openload -l 8 127.0.0.1:5000 4
killall -9 pypy-c-limit-halving

echo "WITHOUT PENALTY, SERIAL"
PYPYLOG=stm-log:forget.log pypy-c-limit-halving app.py 2> /dev/null &
sleep 2
openload -l 8 127.0.0.1:5000 1
killall -9 pypy-c-limit-halving


echo "WITH SHOULD_COMMIT"
PYPYLOG=stm-log:stm_should_commit.log pypy-c-limit-halving app_should_commit.py 2> /dev/null &
sleep 2
openload -l 8 127.0.0.1:5000 4
killall -9 pypy-c-limit-halving


echo "WITH PENALTY"
PYPYLOG=stm-log:stm_penalty.log pypy-c-contention-penalty-no-scenarios app.py 2> /dev/null &
sleep 2
openload -l 8 127.0.0.1:5000 4
killall -9 pypy-c-contention-penalty-no-scenarios
