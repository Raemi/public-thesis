#!/usr/bin/python

from abstract_threading import (Future, atomic)
from stmlist import STMList, NonSTMList

def three_n_plus_1(n):
    steps = 0
    while n > 1:
        n = n / 2 if n & 1 == 0 \
          else n * 3 + 1
        steps += 1
    return steps


def for_set(ns):
    results = []
    with atomic:
        for n in ns:
            results.append(three_n_plus_1(n))

def split_every_n(it, n):
    return (it[i:i+n] for i in xrange(0, len(it), n))

def join(fs):
    for f in fs:
        f()

def run(ths=4, upto=1000, groups=100):
    ths = int(ths)
    upto = int(upto)
    groups = int(groups)
    print ths, upto, groups

    fs = []
    todo = split_every_n(range(upto), groups)

    try:
        while True:
            for _ in xrange(ths):
                ns = todo.next()
                fs.append(Future(for_set, ns))
            join(fs)
    except StopIteration:
        join(fs)



if __name__ == '__main__':
    run()
