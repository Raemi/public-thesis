#!/bin/bash

plot_log.py stm_list_bad.log --legend --no-rma --pdf stm_list_bad.pdf --threads 1-10 --no-should-commit
plot_log.py stm_list_good.log --legend --no-rma --pdf stm_list_good.pdf --threads 1-10 --no-should-commit
