#!/bin/bash

# non-atomic
LD_PRELOAD=/usr/lib/libbfd.so ~/mutrace/mutrace.in --all -d pypy-c-limit-halving ../bench.py -k1 bench.py 32 200000 1000
pypy-c-limit-halving ../bench.py bench.py 32 200000 1000
PYPYLOG=stm-log:stm.log pypy-c-limit-halving ../bench.py -k1 bench.py 32 200000 1000

# atomic:
LD_PRELOAD=/usr/lib/libbfd.so ~/mutrace/mutrace.in --all -d pypy-c-limit-halving ../bench.py -k1 bench_atomic.py 32 200000 1000
pypy-c-limit-halving ../bench.py bench_atomic.py 32 200000 1000
PYPYLOG=stm-log:stm_atomic.log pypy-c-limit-halving ../bench.py -k1 bench_atomic.py 32 200000 1000
