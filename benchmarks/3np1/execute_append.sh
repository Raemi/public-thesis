#!/bin/bash

PYPYLOG=stm-log:stm_append_good.log pypy-c-limit-halving ../bench.py -k1 -p bench_append.py 10 40000 1000 stm
PYPYLOG=stm-log:stm_append_bad.log pypy-c-limit-halving ../bench.py -k1 -p bench_append.py 10 40000 1000 old

PYPYLOG=stm-log:forget.log pypy-c-limit-halving ../bench.py -k1 -p bench_append.py 1 40000 1000 stm
PYPYLOG=stm-log:forget.log pypy-c-limit-halving ../bench.py -k1 -p bench_append.py 1 40000 1000 old
