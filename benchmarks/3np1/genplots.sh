#!/bin/bash
plot_log.py stm.log --threads 1-32 --legend --no-rma --pdf stm.pdf --compress
plot_log.py stm_atomic.log --threads 1-32 --legend --no-rma --pdf stm_atomic.pdf --compress
