#!/bin/bash

plot_log.py stm_append_bad.log --legend --no-rma --pdf stm_append_bad.pdf --threads 1-10 --no-should-commit
plot_log.py stm_append_good.log --legend --no-rma --pdf stm_append_good.pdf --threads 1-10 --no-should-commit
