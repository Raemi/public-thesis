#!/bin/bash
PYPYLOG=stm-log:stm.log pypy-c-limit-halving bench.py -k1 3np1/bench_element_access.py 10 40000 1000
PYPYLOG=stm-log:stm.log pypy-c-limit-halving-list-strategies bench.py -k1 3np1/bench_element_access.py 10 40000 1000

PYPYLOG=stm-log:forget.log pypy-c-limit-halving bench.py -k1 3np1/bench_element_access.py 1 40000 1000
PYPYLOG=stm-log:forget.log pypy-c-limit-halving-list-strategies bench.py -k1 3np1/bench_element_access.py 1 40000 1000
