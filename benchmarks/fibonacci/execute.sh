#!/bin/bash

LD_PRELOAD=/usr/lib/libbfd.so ~/mutrace/mutrace.in --all -d pypy-c-limit-halving fib_parallel.py
PYPYLOG=stm-log:stm.log time -p pypy-c-limit-halving fib_parallel.py
