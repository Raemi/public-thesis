#!/usr/bin/python
import thread
thread.start_new_thread(lambda:0, ())

def fib(n):
    if n <= 1:
        return n
    return fib(n-1) + fib(n-2)


def run():
    print fib(30)

if __name__ == '__main__':
    run()
